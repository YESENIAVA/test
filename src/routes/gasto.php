<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//Obtener gastos
$app->get('/conta/gastos/',function(Request $request, Response $response)
{
	# code...
	getGastos();
});
//Obtener gastos
$app->get('/conta/gastos',function(Request $request, Response $response)
{
	# code...
	getGastos();
});
//Agregar gasto
$app->post('/conta/gasto/',function(Request $request, Response $response)
{
	# code...
	$name = $request->getParam('name');
	$date = $request->getParam('date');
	$monto = $request->getParam('monto');

	addGasto($name, $date, $monto);
});
//Agregar gasto
$app->post('/conta/gasto',function(Request $request, Response $response)
{
	# code...
	$name = $request->getParam('name');
	$date = $request->getParam('date');
	$monto = $request->getParam('monto');

	addGasto($name, $date, $monto);
});
//Actualizar gasto
$app->put('/conta/gasto/{id}',function(Request $request, Response $response)
{
	# code...
	$id = $request->getAttribute('id');
	$name = $request->getParam('name');
	$date = $request->getParam('date');
	$monto = $request->getParam('monto');

	updateGasto($id, $name, $date, $monto);
});
//Eliminar gasto
$app->delete('/conta/gasto/{id}',function(Request $request, Response $response)
{
	# code...
	$id = $request->getAttribute('id');

	deleteGasto($id);
});


//Funciones

function getGastos()
{
	# code...
	$sql = "SELECT * FROM gasto ORDER BY id DESC";

	//Obtener objeto DB
	$db = new db();
	//Conexión
	$dbConn = $db->connect();
	//Ejecucion del query sql
	$result = $dbConn->query($sql);

	if($result->num_rows > 0){
		//Salida de datos de cada columna a json
		$rows = array();
		$i = 0;

		while($row = $result->fetch_assoc()) {
			$rows[$i]['id'] = $row["id"];
			$rows[$i]['name'] = $row["name"];
			$rows[$i]['date'] = $row["date"];
			$rows[$i]['monto'] = $row["monto"];
			$i++;
    	}

		$dbConn->close();
		echo json_encode($rows);
		
	}
}

function addGasto($name, $date, $monto)
{
	# code...
	//Obtener objeto DB
	$db = new db();
	//Conexión
	$dbConn = $db->connect();
	$sql = "Insert into gasto (name, date, monto) Values('$name', '$date', $monto)";
	$stmt = $dbConn->prepare("INSERT INTO gasto (name, date, monto) VALUES (?, ?, ?)");
	$stmt->bind_param('sss', $name, $date, $monto);
	//Ejecucion del query insert sql
	if ($stmt->execute() === TRUE) {
	    echo "ok";
	} else {
	    echo "Error: " . $sql . "<br>" . $dbConn->error;
	}
}

function updateGasto($id, $name, $date, $monto)
{
	# code...
	$sql = "Update gasto Set 
				name  = '$name', 
				date  = '$date',
				monto =  $monto 
			Where id = $id";

	//Obtener objeto DB
	$db = new db();
	//Conexión
	$dbConn = $db->connect();
	//Ejecucion del query update sql
	$stmt = $dbConn->prepare("UPDATE gasto SET 
								name  = ?, 
								date  = ?,
								monto = ? 
							WHERE id = ?");
	$stmt->bind_param('sssd', $name, $date, $monto, $id);
	//Ejecucion del query insert sql
	if ($stmt->execute() === TRUE) {
	    echo "ok";
	} else {
	    echo "Error: " . $sql . "<br>" . $dbConn->error;
	}
}

function deleteGasto($id)
{
	# code...
	$sql = "Delete From gasto Where id = $id";

	//Obtener objeto DB
	$db = new db();
	//Conexión
	$dbConn = $db->connect();
	//Ejecucion del query update sql
	$stmt = $dbConn->prepare("DELETE FROM gasto WHERE id = ?");
	$stmt->bind_param('s', $id);
	//Ejecucion del query insert sql
	if ($stmt->execute() === TRUE) {
	    echo "ok";
	} else {
	    echo "Error: " . $sql . "<br>" . $dbConn->error;
	}
}


?>
