<?php 

	class db{
		//Propiedades
		private $bdhost = '127.0.0.1';
		private $dbport = '3306';
		private $dbuser = 'root';
		private $dbpass = '';
		private $dbname = 'conta';

		//Conexión
		public function connect()
		{
			# code...
			// Create connection
			$conn = new mysqli($this->bdhost,$this->dbuser,$this->dbpass,$this->dbname,$this->dbport);
			//$conn = mysqli_connect($this->$bdhost,$this->$dbuser,$this->$dbpass,$this->$dbname,$this->$dbport);

			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			} 
			//Conexión exitosa
			return $conn;
		}

	}

 ?>